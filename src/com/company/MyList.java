package com.company;

import java.util.Optional;

public class MyList<T> implements SimpleList<T>, AuthorHolder {
    private Link<T> head;
    private Link<T> tail;
    private int size;

    public MyList() {
    }

    @Override
    public void add(T item) {
        Link<T> newItem = new Link<>(item);
        if (isEmpty()) {
            insertFirst(newItem);
        } else {
            insertLast(newItem, tail.index + 1);
        }
        size++;
    }

    @Override
    public void insert(int index, T item) throws Exception {
        if (index <= size && index > 0) {
            Link<T> newItem = new Link<>(index, item);
            if (head.index == index) {
                insertFirst(newItem);
            } else if (tail.index == index) {
                insertBeforeTail(newItem);
            } else {
                insertByIndex(index, newItem);
            }
            size++;
        } else {
            throw new IllegalArgumentException("Неправильный индекс");
        }
    }

    @Override
    public void remove(int index) throws Exception {
        Link<T> current = findByIndex(index);
        if (current == null) {
            throw new IllegalArgumentException("Элемент с данным индексом не найден");
        }
        if (head.index == current.index) {
            removeFirst(current);
        } else if (tail.index == current.index) {
            removeLast(current);
        } else {
            removeByIndex(current);
        }
        size--;
    }

    @Override
    public Optional<T> get(int index) { //получить элемент по индексу
        Link<T> current = this.head;
        while (current != null && current.index != index) {
            current = current.next;
        }
        Optional<Link<T>> currentOpt = Optional.ofNullable(current);
        if (currentOpt.isPresent()) {
            Link<T> tLink = currentOpt.get();
            return Optional.of(tLink.element);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void addAll(SimpleList<T> list) { // Добавить такой же список
        for (int i = 0; i < list.size(); i++) {
            Optional<T> item = list.get(i);
            item.ifPresent((T t) -> add(t));
        }
    }

    @Override
    public int first(T item) {
        Link<T> current = this.head;
        while (current.element != item) {
            current = current.next;
        }
        return current.index;
    }

    @Override
    public int last(T item) {
        Link<T> current = this.tail;
        while (current.element != item) {
            current = current.prev;
        }
        return current.index;
    }

    @Override
    public boolean contains(T item) { // проверка на содержание
        Link<T> current = this.head;
        while (current.element != item) {
            current = current.next;
            if (current == null)
                return false;
        }
        return true;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public String author() {
        return "Akhmetsagirov Ramil";
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Link<T> current = this.tail;
        while (current != null) {
            builder.append(current).append("\n");
            current = current.prev;
        }
        return builder.toString();
    }

    private Link<T> findByIndex(int index) {
        Link<T> current = this.head;
        while (current != null && current.index != index) {
            current = current.next;
        }
        return current;
    }

    private void insertLast(Link<T> newItem, int index) {
        tail.next = newItem;
        newItem.prev = tail;
        newItem.index = index;
        tail = newItem;
    }

    /**
     * Вставка по индексу
     *
     * @param index индекс элемента
     * @param newItem новый элемент
     */
    private void insertByIndex(int index, Link<T> newItem) {
        Link<T> current = findByIndex(index);
        if (current == null) {
            insertLast(newItem, index);
            return;
        }
        Link<T> prevLink = current.prev;
        newItem.next = current;
        newItem.prev = prevLink;
        current.prev = newItem;
        prevLink.next = newItem;
        newItem.index = current.index;
        current.index = newItem.index + 1;
        incIndexOfElements(current);
    }

    /**
     * Вставка перед хвостом
     *
     * @param newItem новый элемент
     */
    private void insertBeforeTail(Link<T> newItem) {
        Link<T> prevItem = tail.prev;
        prevItem.next = newItem;
        newItem.prev = prevItem;
        newItem.next = tail;
        newItem.index = tail.index;
        tail.index++;
    }

    /**
     * Вставить элемент в начало
     *
     * @param newItem
     */
    private void insertFirst(Link<T> newItem) {
        newItem.next = head;
        newItem.index = 0;
        if (!isEmpty()) {
            head.prev = newItem;
        } else {
            tail = newItem;
        }
        head = newItem;
        incIndexOfElements(head);
    }

    /**
     * Увеличить индекс всех элементов от текущего до хвоста на + 1
     *
     * @param current текущий элемент
     */
    private void incIndexOfElements(Link<T> current) {
        while (current != null && current != tail) {
            current = current.next;
            current.index++;
        }
    }

    /**
     * Уменьшить индекс всех элементов от текущего до хвоста на -1
     *
     * @param current текущий элемент
     */
    private void decIndexOfElements(Link<T> current) {
        while (current != null && current != tail) {
            current = current.next;
            current.index--;
        }
    }

    private void removeByIndex(Link<T> current) {
        Link<T> prevItem = current.prev;
        Link<T> nextItem = current.next;
        prevItem.next = nextItem;
        nextItem.prev = prevItem;
        nextItem.index = current.index;

        current.next = null;
        current.prev = null;
        decIndexOfElements(nextItem);
    }

    private void removeLast(Link<T> current) {
        Link<T> prev = current.prev;
        prev.next = null;
        current.prev = null;
    }

    private void removeFirst(Link<T> current) {
        head = current.next;
        head.index = 0;
        current.next = null;
        current.prev = null;
        decIndexOfElements(head);
    }

    private static class Link<T> {
        T element;
        Link<T> next;
        Link<T> prev;
        int index;

        public Link(T item) {
            this.element = item;
        }

        public Link(int index, T item) {
            this.index = index;
            this.element = item;
        }

        @Override
        public String toString() {
            return "Link{" +
                    "element=" + element +
                    ", index=" + index +
                    '}';
        }
    }
}
